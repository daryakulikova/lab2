﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace lab2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string localDataPath = AppDomain.CurrentDomain.BaseDirectory + @"\localThreat.xlsx";
        string tempDataPath = AppDomain.CurrentDomain.BaseDirectory + @"\tempThreat.xlsx";
        double countThreatOnListBox = 10.0;
        List<Threat> listThreat;
        List<Threat> templistThreat;
        int countlistBox;
        List<Button> listButton = new List<Button>();
        int currentPage = 1;
        public MainWindow()
        {
            InitializeComponent();

            if (!File.Exists(localDataPath))
            {
                const string message = "Файл с локальной базой данных на компьютере отсутствует. Хотите провести первичную загрузку данных?";
                const string caption = "Form Closing";
                var result = System.Windows.MessageBox.Show(message, caption,
                                             MessageBoxButton.YesNo,
                                             MessageBoxImage.Question);

                if (result == MessageBoxResult.No)
                {
                    Environment.Exit(0);
                }
                else if (result == MessageBoxResult.Yes)
                {
                    WebClient client = new WebClient();
                    client.DownloadFile("https://bdu.fstec.ru/documents/files/thrlist.xlsx", localDataPath);
                }
            }
            listThreat = Parcer.OpenExcel(localDataPath);
            ListBox listBox = new ListBox();


            countlistBox = (int)Math.Ceiling(listThreat.Count / countThreatOnListBox);

            for (int i = 0; i < countlistBox; i++)
            {
                Button button = new Button();
                button.MinWidth = 30;
                button.Content = i + 1;
                button.Click += Button_Click;
                stackPanel.Children.Add(button);
                listButton.Add(button);
            }
            SetPage(1);

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

            int pageNumber = Convert.ToInt32(((Button)sender).Content.ToString());
            currentPage = pageNumber;
            SetPage(pageNumber);

        }
        public void SetPage(int pageNumber)
        {
            foreach (var item in listButton)
            {
                item.Background = Brushes.White;
            }
            listButton[pageNumber - 1].Background = Brushes.Blue;
            listBox.Items.Clear();
            int lenght = (int)countThreatOnListBox;
            if (pageNumber == countlistBox)
            {
                lenght = listThreat.Count % (int)countThreatOnListBox;
            }
            for (int j = 0; j < lenght; j++)
            {
                ListBoxItem lbi = new ListBoxItem();
                lbi.Uid = j.ToString();
                lbi.MouseDoubleClick += ShowThreat;
                lbi.Content = "УБИ." + listThreat[j + (pageNumber - 1) * (int)countThreatOnListBox].Id + " - " + listThreat[j + (pageNumber - 1) * lenght].Name;
                listBox.Items.Add(lbi);
            }
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            
            List<int> updateId = new List<int>();
            WebClient client = new WebClient();
            client.DownloadFile("https://bdu.fstec.ru/documents/files/thrlist.xlsx", tempDataPath);
            templistThreat = Parcer.OpenExcel(tempDataPath);
            string update = "";
            try
            {                
                for (int i = 0; i < listThreat.Count; i++)
                {
                    if (!listThreat[i].Equals(templistThreat[i]))
                    {
                        updateId.Add(i);
                    }
                }
                update += "Количество обновлённых строк: " + updateId.Count + "\r\n";
                for (int i = 0; i < updateId.Count; i++)
                {
                    update += "УБИ." + (updateId[i] + 1) + "\r\n";
                    if (!String.Equals(listThreat[updateId[i]].Name, templistThreat[updateId[i]].Name, StringComparison.InvariantCultureIgnoreCase))
                    {
                        update += "БЫЛО: " + listThreat[updateId[i]].Name + "\r\nСТАЛО: " + templistThreat[updateId[i]].Name + "\r\n";
                        listThreat[updateId[i]].Name = templistThreat[updateId[i]].Name;
                    }

                    if (!String.Equals(listThreat[updateId[i]].Description, templistThreat[updateId[i]].Description, StringComparison.InvariantCultureIgnoreCase))
                    {
                        update += "БЫЛО: " + listThreat[updateId[i]].Description + "\r\nСТАЛО: " + templistThreat[updateId[i]].Description + "\r\n";
                        listThreat[updateId[i]].Description = templistThreat[updateId[i]].Description;
                    }

                    if (!String.Equals(listThreat[updateId[i]].Source, templistThreat[updateId[i]].Source, StringComparison.InvariantCultureIgnoreCase))
                    {
                        update += "БЫЛО: " + listThreat[updateId[i]].Source + "\r\nСТАЛО: " + templistThreat[updateId[i]].Source + "\r\n";
                        listThreat[updateId[i]].Source = templistThreat[updateId[i]].Source;
                    }

                    if (!String.Equals(listThreat[updateId[i]].AffectedEntity, templistThreat[updateId[i]].AffectedEntity, StringComparison.InvariantCultureIgnoreCase))
                    {
                        update += "БЫЛО: " + listThreat[updateId[i]].AffectedEntity + "\r\nСТАЛО: " + templistThreat[updateId[i]].AffectedEntity + "\r\n";
                        listThreat[updateId[i]].AffectedEntity = templistThreat[updateId[i]].AffectedEntity;
                    }

                    if (listThreat[updateId[i]].ConfidentialityBreach != templistThreat[updateId[i]].ConfidentialityBreach)
                    {
                        update += "БЫЛО: " + listThreat[updateId[i]].ConfidentialityBreach + "\r\nСТАЛО: " + templistThreat[updateId[i]].ConfidentialityBreach + "\r\n";
                        listThreat[updateId[i]].ConfidentialityBreach = templistThreat[updateId[i]].ConfidentialityBreach;
                    }

                    if (listThreat[updateId[i]].IntegrityBreach != templistThreat[updateId[i]].IntegrityBreach)
                    {
                        update += "БЫЛО: " + listThreat[updateId[i]].IntegrityBreach + "\r\nСТАЛО: " + templistThreat[updateId[i]].IntegrityBreach + "\r\n";
                        listThreat[updateId[i]].IntegrityBreach = templistThreat[updateId[i]].IntegrityBreach;
                    }

                    if (listThreat[updateId[i]].AccessibilityBreach != templistThreat[updateId[i]].AccessibilityBreach)
                    {
                        update += "БЫЛО: " + listThreat[updateId[i]].AccessibilityBreach + "\r\nСТАЛО: " + templistThreat[updateId[i]].AccessibilityBreach + "\r\n";
                        listThreat[updateId[i]].AccessibilityBreach = templistThreat[updateId[i]].AccessibilityBreach;
                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "ОШИБКА");
            }
            MessageBox.Show(update,"УСПЕШНО ОБНОВЛЁН");
            SetPage(currentPage);           
        }

        private void ShowThreat(object sender, MouseEventArgs e)
        {            
            int n = Convert.ToInt32(((ListBoxItem)sender).Uid)+(currentPage-1)*(int)countThreatOnListBox;            
            MessageBox.Show(listThreat[n].ToString(), "ВСЕ СВЕДЕНИЯ ОБ УГРОЗЕ");
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            TextWriter tw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + @"\SavedListThreat.txt");
            foreach (var s in listThreat)
                tw.WriteLine(s.ToString());
            tw.Close();
            MessageBox.Show("Список сохранен!");
        }
    }
}
