﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2
{
    class Threat
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Source { get; set; }
        public string AffectedEntity { get; set; }
        public string ConfidentialityBreach { get; set; }
        public string IntegrityBreach { get; set; }
        public string AccessibilityBreach { get; set; }

        public Threat(int id, string name, string description, string source, string affectedEntity, string confidentialityBreach, string integrityBreach, string accessibilityBreach)
        {
            Id = id;
            Name = name;
            Description = description;
            Source = source;
            AffectedEntity = affectedEntity;
            ConfidentialityBreach = confidentialityBreach;
            IntegrityBreach = integrityBreach;
            AccessibilityBreach = accessibilityBreach;
        }

        public override bool Equals(Object obj)
        {
            Threat th = obj as Threat;
            //for (int i = 0; i < this.Description.Length; i++)
            //{
            //    if (this.Description[i] != th.Description[i])
            //    {
            //        return false;
            //    }
            //}
            if (String.Equals(this.Name,th.Name, StringComparison.InvariantCultureIgnoreCase)&&
                String.Equals(this.Source, th.Source, StringComparison.InvariantCultureIgnoreCase) &&
                String.Equals(this.Description,th.Description, StringComparison.InvariantCultureIgnoreCase) &&
                String.Equals(this.AffectedEntity,th.AffectedEntity, StringComparison.InvariantCultureIgnoreCase) &&
                this.ConfidentialityBreach.Equals(th.ConfidentialityBreach) &&
                this.IntegrityBreach.Equals(th.IntegrityBreach) &&
                this.AccessibilityBreach.Equals(th.AccessibilityBreach))
            {
                return true;
            }
            else
            {
                return false;
            }
            
        }

        public override string ToString()
        {
            return "Идентификатор УБИ: " + this.Id + "\r\n" +
                "Наименование УБИ: " + this.Name + "\r\n" +
                "Описание: " + this.Description + "\r\n" +
                "Источник угрозы: " + this.Source + "\r\n" +
                "Объект воздействия: " + this.AffectedEntity + "\r\n" +
                "Нарушение конфиденциальности: " + this.ConfidentialityBreach + "\r\n" +
                "Нарушение целостности: " + this.IntegrityBreach + "\r\n" +
                "Нарушение доступности: " + this.AccessibilityBreach + "\r\n";
        }
    }
}
