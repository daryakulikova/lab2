﻿using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;

namespace lab2
{
    class Parcer
    {
        public static List<Threat> OpenExcel(string path)
        {
            List<Threat> listThreat = new List<Threat>();

            using (SpreadsheetDocument doc = SpreadsheetDocument.Open(path, false))
            {
                WorkbookPart workbookPart = doc.WorkbookPart;
                SharedStringTablePart sstpart = workbookPart.GetPartsOfType<SharedStringTablePart>().First();
                SharedStringTable sst = sstpart.SharedStringTable;

                WorksheetPart worksheetPart = workbookPart.WorksheetParts.First();
                Worksheet sheet = worksheetPart.Worksheet;
                
                List<string> data = new List<string>();
                int i = 0;
  
                var cells = sheet.Descendants<Cell>();
                var rows = sheet.Descendants<Row>();

                foreach (Row row in rows)
                {
                    i++;
                    if (i < 3)
                    {
                        continue;
                    }
                    foreach (Cell c in row.Elements<Cell>())
                    {
                        if ((c.DataType != null) && (c.DataType == CellValues.SharedString))
                        {
                            int ssid = int.Parse(c.CellValue.Text);
                            string str = sst.ChildElements[ssid].InnerText;
                            data.Add(str);

                        }
                        else if (c.CellValue != null)
                        {
                            string str2 = c.CellValue.Text;
                            data.Add(str2);
                        }
                    }
                    
                    data[1] = data[1].Replace(Environment.NewLine, "");
                    data[2] = data[2].Replace(Environment.NewLine, "");
                    data[3] = data[3].Replace(Environment.NewLine, "");
                    data[4] = data[4].Replace(Environment.NewLine, "");                    
                    
                    Threat threat = new Threat(Convert.ToInt32(data[0]), data[1], data[2], data[3], data[4], data[5], data[6], data[7]);
                    data.Clear();
                    listThreat.Add(threat);
                }
                
            }
            return listThreat;
        }
    }
}
